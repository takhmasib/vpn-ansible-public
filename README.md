# Deploy an OpenVPN server on AlmaLinux using Ansible


## Cloning repo

```
git clone https://gitlab.com/takhmasib/openvpn-ansible-public.git
```

## Usage and notes


**Install dependencies**

Requires `jmespath` for `json_query` to work, although (currently) only for cleanup step to work properly.

```
python3 -m pip install jmespath
```

**Deploy**

Deploy openvpn server, by default one client config is created

```
ansible-playbook main.yml
```

**Additional client config**

Generate an additional client config file, by default `newclient01`

```
ansible-playbook -t newclient [-e "newclientname=<newclient_name>"] main.yml
```

**Clean up**

Stop and uninstall openvpn server, clean up created configs

```
ansible-playbook -t cleanup main.yml
```




## Notes and known issues


**Firewalld configuration**

Firewall rules partially applied via 'shell' module, since Ansible's 'firewalld' module does not support configuring direct rules.

**Egress IP**

IP address in the client config is taken from the output of the shell command:

```
ip r | awk '/^default via/{print $9}'
```
That makes the playbook unusable when server is behind NAT.

